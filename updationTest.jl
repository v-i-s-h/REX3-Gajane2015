#!/usr/bin/env julia

################################################################################
# For studying the updation of weight matrix
################################################################################

# Bandit Simulation
using Distributions     # For sampling arms
using Gadfly            # For plotting
using Colors            # Select colors for plotting
using HDF5              # For saving data
using JLD               # For saving data

# Seed RNG
# srand( 14213 );

# Experiment parametres
# Create a Bandit with 10 arms
noOfArms    = 10;
T           = 2000;                       # Number of time steps
# REX3 parametres
γ   = 0.00; # Only now!

μ_arms      = rand( 1, noOfArms );
# println( "Arm rewards : [ ", [@sprintf("%3.2f ",μ_arms[i]) for i=1:noOfArms]... ,"]" )
μ_arms      = [ 0.490 0.495 0.500 0.505 0.510 0.515 0.520 0.525 0.530 0.535 ]
# μ_arms      = [ 0.10 0.20 0.40 0.60 0.80 0.95 ]
μ_optimal   = maximum( μ_arms );        # Optimal Reward
i_optimal   = indmax( μ_arms );         # Optimal Arm index


# Initilal weights for arms = 1
w   = ones( 1, noOfArms );

println( "Arm rewards : [ ", [@sprintf("%4.3f ",μ_arms[i]) for i=1:noOfArms]... ,"]" )

for t = 1:T
    Σw   = sum( w[t,:] );     # Sum of all weights, denominator of probability calculation
    println( "[ t = ", @sprintf("%3d",t), "] : ",
             "w  = [ ", [@sprintf("%4.3f ",w[t,i]) for i=1:noOfArms]..., "]    ",
             "Σw = ", @sprintf("%6.4f",Σw) );
    # Calculate probability for each arm
    # println( "[ t = ", @sprintf("%3d",t), "] : ", "Σw = ", Σw );
    p    = Float64[ (1-γ)*w[t,i]/Σw + γ/noOfArms for i = 1:noOfArms ]; # Calculate probabilty
    println( "             ",
             "p  = [ ", [@sprintf("%4.3f ",p[i]) for i=1:noOfArms]..., "]" );
    armDistribution = Categorical( p );   # Create a distribution with p_t
    # Select two arms
    a_t = rand( armDistribution );
    b_t = rand( armDistribution );
    # Generate Rewards  & Trim them between [0,1]
    x_a = 0.10*randn()+μ_arms[a_t];     x_a = (x_a>0)?((x_a<1.0)?x_a:1.0):0.0;
    x_b = 0.10*randn()+μ_arms[b_t];     x_b = (x_b>0)?((x_b<1.0)?x_b:1.0):0.0;
    println( "             ",
             "Arms selected : (", @sprintf("%2d,%2d",a_t,b_t)") ",
             "Rewards : (", @sprintf("%4.3f,%4.3f",x_a,x_b), ") " );
    # Relative Feedback
    ψ = x_a - x_b;
    w_dash = w[t,:];
    c_hat = zeros( 1, noOfArms );
    if( a_t != b_t )    # If both arms are same, same weight goes to nest round
        # Calculate c_hat as a vector
        c_hat[a_t] = +1 * ψ/(2*p[a_t]);
        c_hat[b_t] = -1 * ψ/(2*p[b_t]);
    end
    # w_dash = w_dash .* exp(γ/noOfArms*c_hat);
    w_dash = w_dash .* exp(c_hat);
    w = [ w; w_dash ];
end
