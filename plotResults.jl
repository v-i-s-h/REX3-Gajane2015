# To plot results from JLD file

using Gadfly            # For plotting
using Colors            # Select colors for plotting
using HDF5              # For saving data
using JLD               # For saving data

resultFile  = "./data_out/exp_regretBounds/regretBounds_1000x0100_0.36_5000";

# Load results
hSimResult  = load( @sprintf("%s.jld",resultFile) );

# Select pen colors
penColors = distinguishable_colors( 6+1 )[2:end];

# Generate xPoints
xPoints = collect( 1:hSimResult["T"] );

print( @sprintf("Plotting the results to %s_1-2-6.pdf ....... ",resultFile) );
figure  = plot(
                layer(
                    x = xPoints,
                    y = hSimResult["avgRegret"],
                    Geom.line,
                    Theme(default_color=penColors[1]) ),
                layer(
                    x = xPoints,
                    y = hSimResult["UB_C1"],
                    Geom.line,
                    Theme(default_color=penColors[2]) ),
                # layer(
                #     x = xPoints,
                #     y = hSimResult["UB_C2"],
                #     Geom.line,
                #     Theme(default_color=penColors[3]) ),
                # layer(
                #     x = xPoints,
                #     y = hSimResult["UB_app"],
                #     Geom.line,
                #     Theme(default_color=penColors[4]) ),
                # layer(
                #     x = xPoints,
                #     y = hSimResult["UB_app2"],
                #     Geom.line,
                #     Theme(default_color=penColors[5]) ),
                layer(
                    x = xPoints,
                    y = hSimResult["UB_app3"],
                    Geom.line,
                    Theme(default_color=penColors[6]) ),
                Guide.xlabel( "Time Steps" ),
                Guide.title( @sprintf("REX3 Improved Regret - With optimal γ \n(#bandits = %d, #arms = %d, γ = %3.2f)",
                                        hSimResult["noOfBandits"], hSimResult["noOfArms"], hSimResult["γ"]) ),
                Guide.manual_color_key( "Legend",
                                        [
                                          "Observed Regret",
                                          "Upper bound based on Cor 4.2.1",
                                        #   "Upper bound based on Cor 4.2.1",
                                        #   "Upper bound based on Thm 5.2"
                                        #   "Upper bound based on Cor 5.2.1",
                                          "Upper bound based on Cor 5.1.1"
                                        ],
                                        [ penColors[1], penColors[2], penColors[6] ] )
          );
draw( PDF(@sprintf("%s_1-2-6.pdf",resultFile),2*136,136), figure );
println( "[DONE]" );
