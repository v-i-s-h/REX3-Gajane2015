#!/usr/bin/env julia

################################################################################
# Simulation for comparing the tightness of the bound
# Compared to Gajane2015-REX3, new term M3 is added and approximated
# Read Section "Improving The Bounds" in notes
################################################################################

# Bandit Simulation
using Distributions     # For sampling arms
using Gadfly            # For plotting
using Colors            # Select colors for plotting
using HDF5              # For saving data
using JLD               # For saving data

# Seed RNG
srand( 14213 );

# Experiment parametres
# Create a Bandit with 10 arms
noOfArms    = 10;
T           = 5000;                       # Number of time steps
# REX3 parametres
γ   = 0.49; # Optimal = ??

μ_arms      = rand( 1, noOfArms );
μ_optimal   = maximum( μ_arms );        # Optimal Reward
i_optimal   = indmax( μ_arms );         # Optimal Arm index


# Initilal weights for arms = 1
w   = ones( 1, noOfArms );

# Debug variables
IM_EQ1_RHS          = zeros( T, 1 );
IM_EQ2_RHS          = zeros( T, 1 );
truncatedEqn16RHS   = zeros( T, 1 );
boundedTruncEqn16   = zeros( T, 1 );
IM_EQ5_LHS          = zeros( T, 1 );
IM_EQ5_RHS          = zeros( T, 1 );
M1  = zeros( T, 1 );
M2  = zeros( T, 1 );
M3  = zeros( T, 1 );

for t = 1:T
    Σw   = sum( w[t,:] );     # Sum of all weights, denominator of probability calculation
    # println( "[ t = ", @sprintf("%3d",t), "] : ",
    #          "w  = [ ", [@sprintf("%4.3f ",w[t,i]) for i=1:noOfArms]..., "]    ",
    #          "Σw = ", @sprintf("%6.4f",Σw) );
    # Calculate probability for each arm
    # println( "[ t = ", @sprintf("%3d",t), "] : ", "Σw = ", Σw );
    p    = Float64[ (1-γ)*w[t,i]/Σw + γ/noOfArms for i = 1:noOfArms ]; # Calculate probabilty
    # println( "             ",
    #          "p  = [ ", [@sprintf("%4.3f ",p[i]) for i=1:noOfArms]..., "]" );
    armDistribution = Categorical( p );   # Create a distribution with p_t
    # Select two arms
    a_t = rand( armDistribution );
    b_t = rand( armDistribution );
    # Generate Rewards  & Trim them between [0,1]
    x_a = 0.10*randn()+μ_arms[a_t];     x_a = (x_a>0)?((x_a<1.0)?x_a:1.0):0.0;
    x_b = 0.10*randn()+μ_arms[b_t];     x_b = (x_b>0)?((x_b<1.0)?x_b:1.0):0.0;
    # println( "             ",
    #          "Arms selected : (", @sprintf("%2d,%2d",a_t,b_t)") ",
    #          "Rewards : (", @sprintf("%4.3f,%4.3f",x_a,x_b), ") " );
    # Relative Feedback
    ψ = x_a - x_b;
    w_dash = w[t,:];
    c_hat = zeros( 1, noOfArms );
    if( a_t != b_t )    # If both arms are same, same weight goes to nest round
        # Calculate c_hat as a vector
        c_hat[a_t] = +1 * ψ/(2*p[a_t]);
        c_hat[b_t] = -1 * ψ/(2*p[b_t]);
    end
    w_dash = w_dash .* exp(γ/noOfArms*c_hat);
    w = [ w; w_dash ];

    # Debug algo
    IM_EQ1_RHS[t,:] = 1/(1-γ) * exp(γ/noOfArms*c_hat) * (p-γ/noOfArms);
    IM_EQ2_RHS[t,:] = 1/(1-γ) * (1+ γ/noOfArms*c_hat + (e-2)*(γ/noOfArms*c_hat).^2) *
                                (p-γ/noOfArms);
    M1[t,:] = -1 * (1/noOfArms) * sum(c_hat);
    M2[t,:] = (1/noOfArms) * (c_hat.^2*p);
    M3[t,:] = (1/noOfArms) * sum(c_hat.^2);
    truncatedEqn16RHS[t,:]  = 1 + (γ^2/noOfArms)/(1-γ)*M1[t,:] + (e-2)*(γ^2/noOfArms)/(1-γ)*M2[t,:]; # -
                                # (e-2)*(γ^2/noOfArms)/(1-γ)*(1/noOfArms)*(γ/noOfArms)*(c_hat*c_hat');
    boundedTruncEqn16[t,:]  = exp( (γ^2/noOfArms)/(1-γ)*M1[t,:] +
                                   (e-2)*(γ^2/noOfArms)/(1-γ)*M2[t,:] );

    # Update γ??
end

################################ DEBUG ALGORITHM ###############################
# Calculate W
W = sum( w, 2 );
Ω = Float64[ W[t+1]/W[t] for t = 1:T ];        # Ratio of weights - LHS of Eqn.16
data = [ Ω IM_EQ1_RHS IM_EQ2_RHS truncatedEqn16RHS boundedTruncEqn16 ];

# For ploting Ineq.38
IM_EQ5_LHS = Float64[ log(W[t+1]/W[1]) for t = 1:T ];
ΣM1        = Float64[ sum(M1[1:t]) for t = 1:T ];
ΣM2        = Float64[ sum(M2[1:t]) for t = 1:T ];
ΣM3        = Float64[ sum(M3[1:t]) for t = 1:T ];
IM_EQ5_RHS_actual       = (γ^2/noOfArms)/(1-γ)*ΣM1 +
                          (e-2)*(γ^2/noOfArms)/(1-γ)*ΣM2 -
                          (e-2)*(γ^2/noOfArms)/(1-γ)*(γ/noOfArms)*ΣM3;
IM_EQ5_RHS_approx_truc  = (γ^2/noOfArms)/(1-γ)*ΣM1 + (e-2)*(γ^2/noOfArms)/(1-γ)*ΣM2;
IM_EQ5_RHS_approx_bound = (γ^2/noOfArms)/(1-γ)*ΣM1 + (e-2)*(γ^2/noOfArms)/(1-γ)*(1-(γ/noOfArms))*ΣM2;

################################### PLOT results ###############################
resultFile  = @sprintf( "./data_out/boundComparison_%04d_%3.2f_%04d", noOfArms, γ, T );
# Save data
save( @sprintf("%s.jld",resultFile),
      # Data points in plot
      "IM_EQ5_LHS", IM_EQ5_LHS,
      "IM_EQ5_RHS_actual", IM_EQ5_RHS_actual,
      "IM_EQ5_RHS_approx_truc", IM_EQ5_RHS_approx_truc,
      "IM_EQ5_RHS_approx_bound", IM_EQ5_RHS_approx_bound );

# penColors   = distinguishable_colors(noOfArms+1)[2:end];
penColors   = distinguishable_colors(4+1)[2:end];     # No Black please
xPoints     = collect(1:T);

figure      = plot(
                    layer(
                        x = xPoints,
                        y = IM_EQ5_LHS,
                        Geom.line,
                        Theme(default_color=penColors[1]) ),
                    layer(
                        x = xPoints,
                        y = IM_EQ5_RHS_actual,
                        Geom.line,
                        Theme(default_color=penColors[2]) ),
                    layer(
                        x = xPoints,
                        y = IM_EQ5_RHS_approx_truc,
                        Geom.line,
                        Theme(default_color=penColors[3]) ),
                    layer(
                        x = xPoints,
                        y = IM_EQ5_RHS_approx_bound,
                        Geom.line,
                        Theme(default_color=penColors[4]) ),
                    Coord.Cartesian(
                        ymin = 0,
                        ymax = maximum([IM_EQ5_LHS IM_EQ5_RHS_actual IM_EQ5_RHS_approx_truc IM_EQ5_RHS_approx_bound])*1.1
                    ),
                    Guide.xlabel( "Time Steps" ),
                    # Guide.ylabel( "Weight of Arm" ),
                    Guide.title( @sprintf( "REX3 - Comparison of bounds (γ=%3.2f)", γ) ),
                    Guide.manual_color_key( "Legend",
                        [ "ln(W_{T+1}/W_{1})",
                          "Actual Bound as per Eqn.34",
                          "Truncated bound used in paper",
                          "New Bound" ],
                        penColors )
              );
draw( PDF(@sprintf("%s.pdf",resultFile),2*136,136), figure );
################################################################################
