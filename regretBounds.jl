#!/usr/bin/env julia

################################################################################
# Simulation for comparing the tightness of the bound
# Compared to Gajane2015-REX3, new term M3 is added and approximated
# Read Section "Improving The Bounds" in notes
################################################################################

# Bandit Simulation
using Distributions     # For sampling arms
using Gadfly            # For plotting
using Colors            # Select colors for plotting
using HDF5              # For saving data
using JLD               # For saving data

# Seed RNG
# srand( 14213 );

# Create a Bandit with K arms
# noOfBandits = 100;    # Set by driver script
# noOfArms    = 10;     # Set by driver script
# T   = 1000;      # Number of time steps   # Set by driver script
# σ   = 0.1;       # Noise variance         # Set by driver script
# REX3 parametres
# γ   = 0.10;     # Optimal = ??    # Set by driver script

# Create Bandit reward structure
μ_arms          = rand( noOfBandits, noOfArms );    # Rewards
μ_opt, idxOpt   = findmax( μ_arms, 2 );             # Optimal rewards & index
experimentSize  = size( μ_arms );
idxOpt          = [ ind2sub(experimentSize,idxOpt[i])[2] for i = 1:noOfBandits ];
μ_bad, idxBad   = findmin( μ_arms, 2 );
idxBad          = [ ind2sub(experimentSize,idxBad[i])[2] for i = 1:noOfBandits ];
# for i = 1:noOfBandits
#     print( @sprintf( "[%2d] -> %5.4f\n", idxOpt[i], μ_opt[i] ) )
# end
# Calculate regret Bounds
t       = collect( 1:T );
τ_app   = (e-2) * t;        # Approximate value for τ
τ_dash  = (e*mean(μ_opt)-(4-e)*mean(μ_bad)) * t;
γ       = min( 1/2, √(noOfArms*log(noOfArms)/((e-2)*T)))
UB_app  = noOfArms/γ*log(noOfArms) + γ*τ_app;
UB_C1   = noOfArms/γ*log(noOfArms) + γ*τ_dash;
UB_app2 = 2 * √(noOfArms*log(noOfArms)*τ_app);
UB_C2   = 2 * √(noOfArms*log(noOfArms)*τ_dash);
# New bounds
UB_app3 = noOfArms/γ*log(noOfArms) + (γ./(2*(1-γ))).*( (e-(e-2)*γ/noOfArms)*mean(μ_opt) - ((4-e)+(e-2)*γ/noOfArms)*mean(μ_bad) ).*t;

println( "    REX Simulation" );
println( "    --------------" );
println( "Simulation Parameters : " );
println( @sprintf("    No.of Bandits : %5d",noOfBandits) );
println( @sprintf("    No.of Arms    : %5d",noOfArms) );
println( @sprintf("    T             : %5d",T) );
println( @sprintf("    γ*(calculated): %5.4f", γ) );

# To calculate regret
regretTable     = zeros( noOfBandits, T );

for banditIdx = 1:noOfBandits
    # Initilal weights for arms = 1
    w   = ones( 1, noOfArms );
    # println( @sprintf( "    Bandit#%d    Optimal : %5.4f @[%02d]",
    #                     banditIdx, μ_opt[banditIdx],idxOpt[banditIdx]) )
    for t = 1:T
        print( @sprintf("\rSimulating Experiment [σ = %3.2f] Bandit#%04d    ", σ, banditIdx) )
        Σw   = sum( w[t,:] );     # Sum of all weights, denominator of probability calculation
        # println( "[ t = ", @sprintf("%3d",t), "] : ",
        #          "w  = [ ", [@sprintf("%4.3f ",w[t,i]) for i=1:noOfArms]..., "]    ",
        #          "Σw = ", @sprintf("%6.4f",Σw) );
        # Calculate probability for each arm
        # println( "[ t = ", @sprintf("%3d",t), "] : ", "Σw = ", Σw );
        p    = Float64[ (1-γ)*w[t,i]/Σw + γ/noOfArms for i = 1:noOfArms ]; # Calculate probabilty
        # println( "             ",
        #          "p  = [ ", [@sprintf("%4.3f ",p[i]) for i=1:noOfArms]..., "]" );
        armDistribution = Categorical( p );   # Create a distribution with p_t
        # Select two arms
        a_t = rand( armDistribution );
        b_t = rand( armDistribution );
        # Generate Rewards  & Trim them between [0,1]
        x_a = σ*randn()+μ_arms[banditIdx,a_t];     x_a = (x_a>0)?((x_a<1.0)?x_a:1.0):0.0;
        x_b = σ*randn()+μ_arms[banditIdx,b_t];     x_b = (x_b>0)?((x_b<1.0)?x_b:1.0):0.0;
        # println( "             ",
        #          "Arms selected : (", @sprintf("%2d,%2d",a_t,b_t)") ",
        #          "Arm Rewards : (", @sprintf("%4.3f,%4.3f",x_a,x_b), ") ",
        #          "Algo Reward : (", @sprintf("%4.3f",0.5*(x_a+x_b)), ") ",
        #          "Instant Regret : (", @sprintf("%5.4f",μ_opt[banditIdx]-(0.5*(x_a+x_b))),") " );
        # Relative Feedback
        ψ = x_a - x_b;
        w_dash = w[t,:];
        c_hat = zeros( 1, noOfArms );
        if( a_t != b_t )    # If both arms are same, same weight goes to nest round
            # Calculate c_hat as a vector
            c_hat[a_t] = +1 * ψ/(2*p[a_t]);
            c_hat[b_t] = -1 * ψ/(2*p[b_t]);
        end
        w_dash = w_dash .* exp(γ/noOfArms*c_hat);
        w = [ w; w_dash ];
        # Update γ??

        # For regret calculation
        rewardThisRound  = 0.5 * (x_a+x_b);
        regretThisRound  = μ_opt[banditIdx] - rewardThisRound;
        regretTable[banditIdx,t]    = regretThisRound;
    end
end

# Calculate cummulative regret
cummRegret  = cumsum( regretTable, 2 );
avgRegret   = mean( cummRegret, 1 );

# Plot regret
resultFile  = @sprintf( "./data_out/exp_regretBounds/regretBounds_%04dx%04d_%3.2f_%04d", noOfBandits, noOfArms, γ, T );
penColors   = distinguishable_colors(6+1)[2:end];
xPoints     = collect(1:T);

# Save simulation data
print( @sprintf("\nSaving simulation results to %s.jld ....... ",resultFile) );
save( @sprintf("%s.jld",resultFile),
        "noOfBandits", noOfBandits,
        "noOfArms", noOfArms,
        "T", T,
        "γ", γ,
        "avgRegret", avgRegret,
        "mu_arms", μ_arms,
        "UB_app", UB_app,
        "UB_C1", UB_C1,
        "UB_app2", UB_app2,
        "UB_C2", UB_C2,
        "UB_app3", UB_app3 );
println( "[DONE]" );

print( @sprintf("Plotting the results to %s.pdf ....... ",resultFile) );
figure  = plot(
                layer(
                    x = xPoints,
                    y = avgRegret,
                    Geom.line,
                    Theme(default_color=penColors[1]) ),
                layer(
                    x = xPoints,
                    y = UB_C1,
                    Geom.line,
                    Theme(default_color=penColors[2]) ),
                layer(
                    x = xPoints,
                    y = UB_C2,
                    Geom.line,
                    Theme(default_color=penColors[3]) ),
                layer(
                    x = xPoints,
                    y = UB_app,
                    Geom.line,
                    Theme(default_color=penColors[4]) ),
                layer(
                    x = xPoints,
                    y = UB_app2,
                    Geom.line,
                    Theme(default_color=penColors[5]) ),
                layer(
                    x = xPoints,
                    y = UB_app3,
                    Geom.line,
                    Theme(default_color=penColors[6]) ),
                Guide.xlabel( "Time Steps" ),
                Guide.title( @sprintf("REX3 Avg. Cummulative Regret \n(#bandits = %d, #arms = %d, γ = %3.2f)",
                                        noOfBandits, noOfArms, γ) ),
                # Coord.Cartesian(
                #     ymin = 0,
                #     ymax = max(maximum(sigma_avgRegret[:,T]),Int(T*0.3))
                # ),
                Guide.manual_color_key( "Legend",
                                        [ "Observed Regret",
                                          "Upper Bound based on Corollary 1",
                                          "Upper Bound based on Corollary 2",
                                          "Approx Upper Bound based on Theorem 1",
                                          "Approx Upper Bound based on Corollary 1",
                                          "Upper bound based without approximation"
                                        ],
                                        penColors )
          );
draw( PDF(@sprintf("%s.pdf",resultFile),2*136,136), figure );
println( "[DONE]" );
