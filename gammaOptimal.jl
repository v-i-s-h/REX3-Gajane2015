# To plot the effect of γ
using Gadfly
using Colors

N = 1000;
K = 100;
μ_arms  = rand( N, K );
μ_opt   = maximum( μ_arms, 2 );
μ_bad   = minimum( μ_arms, 2 );
Gmax    = mean( μ_opt )
Gmin    = mean( μ_bad )

println( @sprintf( "Gmax = %5.4f    Gmin = %5.4f", Gmax, Gmin ) );

γ = 0.01:0.001:0.999;


x0      = ( ((4-e)+0            ) * Gmin );
x1      = ( ((4-e)+(2*γ/K)*(e-2)) * Gmin );
bound0  = K./γ*log(K) + (γ/2)./(1-γ).*(e*Gmax - x0);
bound1  = K./γ*log(K) + (γ/2)./(1-γ).*(e*Gmax - x1);
bound2  = K./γ*log(K) + γ .* (e*Gmax - x0);
bound3  = K./γ*log(K) + (γ/2)./(1-γ).*( (e-(e-2)*γ/K)*Gmax - ((4-e)+(e-2)*γ/K)*Gmin );

penColors = distinguishable_colors( 5+1 )[2:end];

p = vstack(
        plot( x = γ, y = bound0, Geom.line,
              Guide.xlabel("γ"), Guide.ylabel("ϕ(γ)"),
              Guide.title( "KlogK/γ + (γ/2)/(1-γ)[eGmax - (4-e)Gmin]") ),
        plot( x = γ, y = bound1, Geom.line,
              Guide.xlabel("γ"), Guide.ylabel("ϕ(γ)"),
              Guide.title( "KlogK/γ + (γ/2)/(1-γ)[eGmax - ((4-e)+(2*γ/K)*(e-2))*Gmin]") ),
        plot( x = γ, y = bound2, Geom.line,
              Guide.xlabel("γ"), Guide.ylabel("ϕ(γ)"),
              Guide.title( "KlogK/γ + γ [eGmax - (4-e)Gmin]" ) ),
        plot( x = γ, y = bound3, Geom.line,
              Guide.title( "KlogK/γ + (γ/2)/(1-γ)[(e-(e-2)γ/K)Gmax - ((4-e)+(e-2)γ/K)Gmin]"))
    );
Gadfly.draw( PDF("./data_out/gammaOptimal.pdf",1.5*136,2*136), p );

# p = plot(
#             layer(
#                 x = γ,
#                 y = bound0,
#                 Geom.line,
#                 Theme(default_color=penColors[1] )
#             ),
#             layer(
#                 x = γ,
#                 y = bound1,
#                 Geom.line,
#                 Theme(default_color=penColors[2] )
#             ),
#             layer(
#                 x = γ,
#                 y = bound2,
#                 Geom.line,
#                 Theme(default_color=penColors[3] )
#             ),
#             Guide.xlabel( "γ" ),
#             Guide.ylabel( "ϕ(γ)" ),
#             Guide.manual_color_key( "Legend",
#                 [ "Regret in paper without approximation",
#                   "New Regret without approximation",
#                   "Orginal Regret in paper with approximation" ],
#                 penColors[1:3] )
#         );
