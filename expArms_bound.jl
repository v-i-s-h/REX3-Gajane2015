#!/usr/bin/env julia

################################################################################
# Simulation for comparing the tightness of the bound
# Compared to Gajane2015-REX3, new term M3 is added and approximated
# Read Section "Improving The Bounds" in notes
################################################################################

# Bandit Simulation
using Distributions     # For sampling arms
using Gadfly            # For plotting
using Colors            # Select colors for plotting
using HDF5              # For saving data
using JLD               # For saving data

# Seed RNG - Fix this for a consistent experiment.
srand( 14213 );

# Experiment parametres
# Create a Bandit with 10 arms
armsRange       = collect(50:50:1000);           # Column vector
T_checkPoints   = [ 1000, 2000, 3000, 4000, 5000 ];   # Column vector
# REX3 parametres
γ   = 0.10; # Optimal = ??

T = maximum( T_checkPoints );

arr_IM_EQ5_LHS              = Float64[];
arr_IM_EQ5_RHS_actual       = Float64[];
arr_IM_EQ5_RHS_approx_truc  = Float64[];
arr_IM_EQ5_RHS_approx_bound = Float64[];

for noOfArms = armsRange
    println( @sprintf("\rSimulating Experiment with #arms = %4d",noOfArms) )
    μ_arms      = rand( 1, noOfArms );
    μ_optimal   = maximum( μ_arms );        # Optimal Reward
    i_optimal   = indmax( μ_arms );         # Optimal Arm index

    # Initilal weights for arms = 1
    w   = ones( 1, noOfArms );

    # Debug variables
    M1  = zeros( T, 1 );
    M2  = zeros( T, 1 );
    M3  = zeros( T, 1 );

    for t = 1:T
        Σw   = sum( w[t,:] );     # Sum of all weights, denominator of probability calculation
        p    = Float64[ (1-γ)*w[t,i]/Σw + γ/noOfArms for i = 1:noOfArms ]; # Calculate probabilty
        armDistribution = Categorical( p );   # Create a distribution with p_t
        # Select two arms
        a_t = rand( armDistribution );
        b_t = rand( armDistribution );
        # Generate Rewards  & Trim them between [0,1]
        x_a = 0.10*randn()+μ_arms[a_t];     x_a = (x_a>0)?((x_a<1.0)?x_a:1.0):0.0;
        x_b = 0.10*randn()+μ_arms[b_t];     x_b = (x_b>0)?((x_b<1.0)?x_b:1.0):0.0;
        # Relative Feedback
        ψ = x_a - x_b;
        w_dash = w[t,:];
        c_hat = zeros( 1, noOfArms );
        if( a_t != b_t )    # If both arms are same, same weight goes to nest round
            # Calculate c_hat as a vector
            c_hat[a_t] = +1 * ψ/(2*p[a_t]);
            c_hat[b_t] = -1 * ψ/(2*p[b_t]);
        end
        w_dash = w_dash .* exp(γ/noOfArms*c_hat);
        w = [ w; w_dash ];

        M1[t,:] = -1 * (1/noOfArms) * sum(c_hat);
        M2[t,:] = (1/noOfArms) * (c_hat.^2*p);
        M3[t,:] = (1/noOfArms) * sum(c_hat.^2);

        # Log data at check points
        if( t in T_checkPoints )
            ΣM1        = sum(M1[1:t]);
            ΣM2        = sum(M2[1:t]);
            ΣM3        = sum(M3[1:t]);
            W          = sum( w, 2 );
            IM_EQ5_LHS = log(W[t+1]/W[1])
            IM_EQ5_RHS_actual       = (γ^2/noOfArms)/(1-γ)*ΣM1 +
                                      (e-2)*(γ^2/noOfArms)/(1-γ)*ΣM2 -
                                      (e-2)*(γ^2/noOfArms)/(1-γ)*(γ/noOfArms)*ΣM3;
            IM_EQ5_RHS_approx_truc  = (γ^2/noOfArms)/(1-γ)*ΣM1 + (e-2)*(γ^2/noOfArms)/(1-γ)*ΣM2;
            IM_EQ5_RHS_approx_bound = (γ^2/noOfArms)/(1-γ)*ΣM1 + (e-2)*(γ^2/noOfArms)/(1-γ)*(1-(γ/noOfArms))*ΣM2;
            # println( "        ", [ IM_EQ5_LHS ΣM1 ΣM2 ΣM3 IM_EQ5_RHS_actual IM_EQ5_RHS_approx_truc IM_EQ5_RHS_approx_bound ] );
            # Save these points
            push!( arr_IM_EQ5_LHS, IM_EQ5_LHS );
            push!( arr_IM_EQ5_RHS_actual, IM_EQ5_RHS_actual );
            push!( arr_IM_EQ5_RHS_approx_truc, IM_EQ5_RHS_approx_truc );
            push!( arr_IM_EQ5_RHS_approx_bound, IM_EQ5_RHS_approx_bound );
        end
    end
end

# Reshape arrays to make columns corresponding to each noOfArms in armsRange and
# rows to each tim echeck points
arr_IM_EQ5_LHS              = reshape( arr_IM_EQ5_LHS,              size(T_checkPoints,1), size(armsRange,1) );
arr_IM_EQ5_RHS_actual       = reshape( arr_IM_EQ5_RHS_actual,       size(T_checkPoints,1), size(armsRange,1) );
arr_IM_EQ5_RHS_approx_truc  = reshape( arr_IM_EQ5_RHS_approx_truc,  size(T_checkPoints,1), size(armsRange,1) );
arr_IM_EQ5_RHS_approx_bound = reshape( arr_IM_EQ5_RHS_approx_bound, size(T_checkPoints,1), size(armsRange,1) );

resultFile  = @sprintf( "./data_out/exp_boundEvo_wArms/expArms_bound_K%04d_T%04d", armsRange[end], T_checkPoints[end]  );

# save data
save( @sprintf("%s.jld",resultFile),
      "armsRange", armsRange,
      "T_checkPoints", T_checkPoints,
      "arr_IM_EQ5_LHS", arr_IM_EQ5_LHS,
      "arr_IM_EQ5_RHS_actual", arr_IM_EQ5_RHS_actual,
      "arr_IM_EQ5_RHS_approx_truc", arr_IM_EQ5_RHS_approx_truc,
      "arr_IM_EQ5_RHS_approx_bound", arr_IM_EQ5_RHS_approx_bound );

# to plot
penColors   = distinguishable_colors( 4+1 )[2:end];
figure      = [];   # Array to hold all plots
for tIndex =  1:size(T_checkPoints,1)
    figure  = plot(
                    layer(
                        x = armsRange,
                        y = arr_IM_EQ5_LHS[tIndex,:],
                        Geom.line,
                        Theme(default_color=penColors[1])   ),
                    layer(
                        x = armsRange,
                        y = arr_IM_EQ5_RHS_actual[tIndex,:],
                        Geom.line,
                        Theme(default_color=penColors[2])   ),
                    layer(
                        x = armsRange,
                        y = arr_IM_EQ5_RHS_approx_truc[tIndex,:],
                        Geom.line,
                        Theme(default_color=penColors[3])   ),
                    layer(
                        x = armsRange,
                        y = arr_IM_EQ5_RHS_approx_bound[tIndex,:],
                        Geom.line,
                        Theme(default_color=penColors[4])   ),
                    Coord.Cartesian(
                        xmin = 0,
                        ymax = maximum( [ maximum(arr_IM_EQ5_LHS),
                                          maximum(arr_IM_EQ5_RHS_actual),
                                          maximum(arr_IM_EQ5_RHS_approx_truc),
                                          maximum(arr_IM_EQ5_RHS_approx_truc) ] )
                    ),
                    Guide.xlabel( "Number of Arms" ),
                    Guide.title( @sprintf( "REX3 - Bound evolution with arms (T=%4d) (γ=%3.2f)", T_checkPoints[tIndex], γ) ),
                    Guide.manual_color_key( "Legend",
                                            [ "ln(W_{T+1}/W_{1})",
                                              "Actual Bound as per Eqn.34",
                                              "Truncated bound used in paper",
                                              "New Bound" ],
                                            penColors )
              );
    draw( PDF(@sprintf("%s_t%04d.pdf",resultFile,T_checkPoints[tIndex]),2*136,136), figure );
end
