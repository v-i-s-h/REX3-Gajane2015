#!/usr/bin/env julia

################################################################################
# Simulation for comparing the tightness of the bound
# Compared to Gajane2015-REX3, new term M3 is added and approximated
# Read Section "Improving The Bounds" in notes
################################################################################

# Bandit Simulation
using Distributions     # For sampling arms
using Gadfly            # For plotting
using Colors            # Select colors for plotting
using HDF5              # For saving data
using JLD               # For saving data

# Seed RNG
# srand( 14213 );

# Create a Bandit with K arms
# noOfBandits = 100;    # Set by driver script
# noOfArms    = 10;     # Set by driver script
# T   = 1000;      # Number of time steps   # Set by driver script
noiseStdDev = [ 0.1 ]

# REX3 parametres
# γ   = 0.10;     # Optimal = ??    # Set by driver script

# Create Bandit reward structure
μ_arms          = rand( noOfBandits, noOfArms );    # Rewards
μ_opt, idxOpt   = findmax( μ_arms, 2 );             # Optimal rewards & index
experimentSize  = size( μ_arms );
idxOpt          = [ ind2sub(experimentSize,idxOpt[i])[2] for i = 1:noOfBandits ];
μ_bad, idxBad   = findmin( μ_arms, 2 );
idxBad          = [ ind2sub(experimentSize,idxBad[i])[2] for i = 1:noOfBandits ];
# for i = 1:noOfBandits
#     print( @sprintf( "[%2d] -> %5.4f\n", idxOpt[i], μ_opt[i] ) )
# end

# To store avgRegret over different σs
nσ  =   length( noiseStdDev );
sigma_avgRegret    = Array( Float64, (0,T) );  # Empty matrix with T columns

println( "    REX Simulation" );
println( "    --------------" );
println( "Simulation Parameters : " );
println( @sprintf("    No.of Bandits : %5d",noOfBandits) );
println( @sprintf("    No.of Arms    : %5d",noOfArms) );
println( @sprintf("    T             : %5d",T) );

for σ in noiseStdDev
    # To calculate regret
    regretTable     = zeros( noOfBandits, T );

    for banditIdx = 1:noOfBandits
        # Initilal weights for arms = 1
        w   = ones( 1, noOfArms );
        # println( @sprintf( "    Bandit#%d    Optimal : %5.4f @[%02d]",
        #                     banditIdx, μ_opt[banditIdx],idxOpt[banditIdx]) )
        for t = 1:T
            print( @sprintf("\rSimulating Experiment [σ = %3.2f] Bandit#%04d    ", σ, banditIdx) )
            Σw   = sum( w[t,:] );     # Sum of all weights, denominator of probability calculation
            # println( "[ t = ", @sprintf("%3d",t), "] : ",
            #          "w  = [ ", [@sprintf("%4.3f ",w[t,i]) for i=1:noOfArms]..., "]    ",
            #          "Σw = ", @sprintf("%6.4f",Σw) );
            # Calculate probability for each arm
            # println( "[ t = ", @sprintf("%3d",t), "] : ", "Σw = ", Σw );
            p    = Float64[ (1-γ)*w[t,i]/Σw + γ/noOfArms for i = 1:noOfArms ]; # Calculate probabilty
            # println( "             ",
            #          "p  = [ ", [@sprintf("%4.3f ",p[i]) for i=1:noOfArms]..., "]" );
            armDistribution = Categorical( p );   # Create a distribution with p_t
            # Select two arms
            a_t = rand( armDistribution );
            b_t = rand( armDistribution );
            # Generate Rewards  & Trim them between [0,1]
            x_a = σ*randn()+μ_arms[banditIdx,a_t];     x_a = (x_a>0)?((x_a<1.0)?x_a:1.0):0.0;
            x_b = σ*randn()+μ_arms[banditIdx,b_t];     x_b = (x_b>0)?((x_b<1.0)?x_b:1.0):0.0;
            # println( "             ",
            #          "Arms selected : (", @sprintf("%2d,%2d",a_t,b_t)") ",
            #          "Arm Rewards : (", @sprintf("%4.3f,%4.3f",x_a,x_b), ") ",
            #          "Algo Reward : (", @sprintf("%4.3f",0.5*(x_a+x_b)), ") ",
            #          "Instant Regret : (", @sprintf("%5.4f",μ_opt[banditIdx]-(0.5*(x_a+x_b))),") " );
            # Relative Feedback
            ψ = x_a - x_b;
            w_dash = w[t,:];
            c_hat = zeros( 1, noOfArms );
            if( a_t != b_t )    # If both arms are same, same weight goes to nest round
                # Calculate c_hat as a vector
                c_hat[a_t] = +1 * ψ/(2*p[a_t]);
                c_hat[b_t] = -1 * ψ/(2*p[b_t]);
            end
            w_dash = w_dash .* exp(γ/noOfArms*c_hat);
            w = [ w; w_dash ];
            # Update γ??

            # For regret calculation
            rewardThisRound  = 0.5 * (x_a+x_b);
            regretThisRound  = μ_opt[banditIdx] - rewardThisRound;
            regretTable[banditIdx,t]    = regretThisRound;
        end
    end

    # Calculate cummulative regret
    cummRegret  = cumsum( regretTable, 2 );
    avgRegret   = mean( cummRegret, 1 );

    # push this to final result
    sigma_avgRegret = vcat( sigma_avgRegret, avgRegret );
end
# Plot regret
resultFile  = @sprintf( "./data_out/exp_regretEvo/regretEvo_%04dx%04d_%3.2f_%04d", noOfBandits, noOfArms, γ, T );
penColors   = distinguishable_colors(size(noiseStdDev,2)+1)[2:end];
xPoints     = collect(1:T);

# Save simulation data
print( @sprintf("\nSaving simulation results to %s ....... ",resultFile) );
save( @sprintf("%s.jld",resultFile),
        "noOfBandits", noOfBandits,
        "noOfArms", noOfArms,
        "T", T,
        "γ", γ,
        "noiseStdDev", noiseStdDev,
        "sigma_avgRegret", sigma_avgRegret );
println( "[DONE]" );
figure  = plot(
                [ layer(
                    x = xPoints,
                    y = sigma_avgRegret[i,:],
                    Geom.line,
                    Theme(default_color=penColors[i]) ) for i = 1:size(noiseStdDev,2) ]...,
                Guide.xlabel( "Time Steps" ),
                Guide.title( @sprintf("REX3 Avg. Cummulative Regret \n(#bandits = %d, #arms = %d, γ = %3.2f)",
                                        noOfBandits, noOfArms, γ) ),
                Coord.Cartesian(
                    ymin = 0,
                    ymax = max(maximum(sigma_avgRegret[:,T]),Int(T*0.3))
                ),
                Guide.manual_color_key( "Legend",
                                        [ @sprintf("σ = %3.2f",noiseStdDev[i]) for i=1:size(noiseStdDev,2) ],
                                        penColors )
          );
draw( PDF(@sprintf("%s.pdf",resultFile),2*136,136), figure );

# ################################ DEBUG ALGORITHM ###############################
# # Calculate W
# W = sum( w, 2 );
# Ω = Float64[ W[t+1]/W[t] for t = 1:T ];        # Ratio of weights - LHS of Eqn.16
# data = [ Ω IM_EQ1_RHS IM_EQ2_RHS truncatedEqn16RHS boundedTruncEqn16 ];
#
# # For ploting Ineq.38
# IM_EQ5_LHS = Float64[ log(W[t+1]/W[1]) for t = 1:T ];
# ΣM1        = Float64[ sum(M1[1:t]) for t = 1:T ];
# ΣM2        = Float64[ sum(M2[1:t]) for t = 1:T ];
# ΣM3        = Float64[ sum(M3[1:t]) for t = 1:T ];
# IM_EQ5_RHS_actual       = (γ^2/noOfArms)/(1-γ)*ΣM1 +
#                           (e-2)*(γ^2/noOfArms)/(1-γ)*ΣM2 -
#                           (e-2)*(γ^2/noOfArms)/(1-γ)*(γ/noOfArms)*ΣM3;
# IM_EQ5_RHS_approx_truc  = (γ^2/noOfArms)/(1-γ)*ΣM1 + (e-2)*(γ^2/noOfArms)/(1-γ)*ΣM2;
# IM_EQ5_RHS_approx_bound = (γ^2/noOfArms)/(1-γ)*ΣM1 + (e-2)*(γ^2/noOfArms)/(1-γ)*(1-(γ/noOfArms))*ΣM2;
#
# ################################### PLOT results ###############################
# resultFile  = "./data_out/boundComparison";
# # penColors   = distinguishable_colors(noOfArms+1)[2:end];
# penColors   = distinguishable_colors(4+1)[2:end];     # No Black please
# xPoints     = collect(1:T);
#
# figure      = plot(
#                     layer(
#                         x = xPoints,
#                         y = IM_EQ5_LHS,
#                         Geom.line,
#                         Theme(default_color=penColors[1]) ),
#                     layer(
#                         x = xPoints,
#                         y = IM_EQ5_RHS_actual,
#                         Geom.line,
#                         Theme(default_color=penColors[2]) ),
#                     layer(
#                         x = xPoints,
#                         y = IM_EQ5_RHS_approx_truc,
#                         Geom.line,
#                         Theme(default_color=penColors[3]) ),
#                     layer(
#                         x = xPoints,
#                         y = IM_EQ5_RHS_approx_bound,
#                         Geom.line,
#                         Theme(default_color=penColors[4]) ),
#                     Coord.Cartesian(
#                         ymin = 0,
#                         ymax = maximum([IM_EQ5_LHS IM_EQ5_RHS_actual IM_EQ5_RHS_approx_truc IM_EQ5_RHS_approx_bound])*1.1
#                     ),
#                     Guide.xlabel( "Time Steps" ),
#                     # Guide.ylabel( "Weight of Arm" ),
#                     Guide.title( @sprintf( "REX3 - Comparison of bounds (γ=%3.2f)", γ) ),
#                     Guide.manual_color_key( "Legend",
#                         [ "ln(W_{T+1}/W_{1})",
#                           "Actual Bound as per Eqn.34",
#                           "Truncated bound used in paper",
#                           "New Bound" ],
#                         penColors )
#               );
# draw( PDF(@sprintf("%s.pdf",resultFile),2*136,136), figure );
# ################################################################################
