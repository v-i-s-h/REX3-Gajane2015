# Driver script for running simulation

# Experiment - 1
noOfBandits =  1000;
noOfArms    =   100;
T           =  6000;
# γ           = 0.25;
σ           = 0.10;
println( @sprintf("Experiment - 1 started at %s",now()) );
@time include( "regretBounds.jl" )
gc();   # Do a little garbage collection

# # Experiment - 2
# noOfBandits =  1000;
# noOfArms    =   200;
# T           =  2500;
# γ           = 0.10;
# println( @sprintf("Experiment - 2 started at %s",now()) );
# @time include( "regretEvo.jl" )
# gc();   # Do a little garbage collection
#
# # Experiment - 3
# noOfBandits =  1000;
# noOfArms    =   300;
# T           =  2500;
# γ           = 0.10;
# println( @sprintf("Experiment - 3 started at %s",now()) );
# @time include( "regretEvo.jl" )
# gc();   # Do a little garbage collection
#
# # Experiment - 4
# noOfBandits =  1000;
# noOfArms    =   400;
# T           =  2500;
# γ           = 0.10;
# println( @sprintf("Experiment - 4 started at %s",now()) );
# @time include( "regretEvo.jl" )
# gc();   # Do a little garbage collection
#
# # Experiment - 5
# noOfBandits =  1000;
# noOfArms    =   500;
# T           =  2500;
# γ           = 0.10;
# println( @sprintf("Experiment - 5 started at %s",now()) );
# @time include( "regretEvo.jl" )
# gc();   # Do a little garbage collection
